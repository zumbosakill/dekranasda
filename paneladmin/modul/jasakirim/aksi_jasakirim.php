<?php
session_start();
require '../FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
include "../../../config/koneksi.php";
include "../../../config/library.php";
include "../../../config/fungsi_thumb.php";
include "../../../config/fungsi_seo.php";

//$p=$_GET['p'];
$act=$_GET['act'];

// Hapus produk
if ($act=='hapus'){
	$data=mysql_fetch_array(mysql_query("SELECT gambar FROM shop_pengiriman WHERE id_perusahaan='$_GET[id]'"));
	if ($data['gambar']!=''){
		$hasil = mysql_query("DELETE FROM shop_pengiriman WHERE id_perusahaan='$_GET[id]'");
		unlink("../../../foto_banner/$_GET[namafile]");
		if($hasil){
			$msg->success("Data berhasil hapus!");
			echo "sukses";
		}else{
			$msg->error("Data gagal hapus!");
			echo "gagal";
		}
	}else{
		$hasil = mysql_query("DELETE FROM shop_pengiriman WHERE id_perusahaan='$_GET[id]'");
		if($hasil){
			$msg->success("Data berhasil hapus!");
			echo "sukses";
		}else{
			$msg->error("Data gagal hapus!");
			echo "gagal";
		}
	}
}

// Input jasa kirim
elseif ($act=='input'){
	$lokasi_file    = $_FILES['fupload']['tmp_name'];
	$tipe_file      = $_FILES['fupload']['type'];
	$nama_file      = $_FILES['fupload']['name'];
	$acak           = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 


	// Apabila ada gambar yang diupload
	if (!empty($lokasi_file)){
		if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" && $tipe_file != "image/png"){
			$msg->error("Upload Gagal, Pastikan File yang di Upload bertipe *.JPG atau *.PNG");
			header('location:../../media.php?p=jasakirim&aksi=tambah');
		}else{
			if($_POST['nama_perusahaan'] != "" && $_POST['alias'] != ""){
				UploadBanner($nama_file_unik);

				$hasil = mysql_query("INSERT INTO shop_pengiriman(nama_perusahaan,
								alias,
								gambar) 
								VALUES('$_POST[nama_perusahaan]',
								'$_POST[alias]',
								'$nama_file_unik')");
				if($hasil){
					$msg->success("Data berhasil disimpan!");
					header('location:../../media.php?p=jasakirim');
				}else{
					$msg->error("Data gagal disimpan!");
					header('location:../../media.php?p=jasakirim&aksi=tambah');
				}
			}else{
				$msg->error("Maaf, Data harus disi lengkap!");
				header('location:../../media.php?p=jasakirim&aksi=tambah');
			}
		}
	}else{
		if($_POST['nama_perusahaan'] != "" && $_POST['alias'] != ""){
			$hasil = mysql_query("INSERT INTO shop_pengiriman(nama_perusahaan,
							alias) 
							VALUES('$_POST[nama_perusahaan]',
							'$_POST[alias]')");
			if($hasil){
				$msg->success("Data berhasil disimpan!");
				header('location:../../media.php?p=jasakirim');
			}else{
				$msg->error("Data gagal disimpan!");
				header('location:../../media.php?p=jasakirim&aksi=tambah');
			}
		}else{
			$msg->error("Maaf, Data harus disi lengkap!");
			header('location:../../media.php?p=jasakirim&aksi=tambah');
		}
	}
}

// Update produk
elseif ($act=='update'){
	$lokasi_file    = $_FILES['fupload']['tmp_name'];
	$tipe_file      = $_FILES['fupload']['type'];
	$nama_file      = $_FILES['fupload']['name'];
	$acak           = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 


	// Apabila gambar tidak diganti
	if (empty($lokasi_file)){
		$hasil = mysql_query("UPDATE shop_pengiriman SET nama_perusahaan = '$_POST[nama_perusahaan]',
						alias           = '$_POST[alias]'
						WHERE id_perusahaan  = '$_POST[id]'");
		if($hasil){
			$msg->success("Data berhasil diubah!");
			header('location:../../media.php?p=jasakirim');
		}else{
			$msg->error("Data gagal diubah!");
			header('location:../../media.php?p=jasakirim');
		}
	}else{
		if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" && $tipe_file != "image/png"){
			$msg->error("Upload Gagal, Pastikan File yang di Upload bertipe *.JPG atau *.PNG");
			header('location:../../media.php?p=jasakirim');
		}else{
			$data=mysql_fetch_array(mysql_query("SELECT gambar FROM shop_pengiriman WHERE id_perusahaan='$_POST[id]'"));
			UploadBanner($nama_file_unik);
			if ($data['gambar'] != ''){
				unlink("../../../foto_banner/$data[gambar]");
			}
			$hasil = mysql_query("UPDATE shop_pengiriman SET nama_perusahaan = '$_POST[nama_perusahaan]',
					alias           = '$_POST[alias]',
					gambar         			= '$nama_file_unik'
					WHERE id_perusahaan  = '$_POST[id]'");
			if($hasil){
				$msg->success("Data berhasil diubah!");
				header('location:../../media.php?p=jasakirim');
			}else{
				$msg->error("Data gagal diubah!");
				header('location:../../media.php?p=jasakirim');
			}
		}
		}
	}
}
?>
