<?php
session_start();
require '../FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}
else{
include "../../../config/koneksi.php";
include "../../../config/library.php";
include "../../../config/fungsi_thumb.php";
include "../../../config/fungsi_seo.php";

//$p=$_GET[p];
$act=$_GET['act'];

// Hapus produk
if ($act=='hapus'){
  $data=mysql_fetch_array(mysql_query("SELECT gambar FROM produk WHERE id_produk='$_GET[id]'"));
  if ($data['gambar']!=''){
     $hasil = mysql_query("DELETE FROM produk WHERE id_produk='$_GET[id]'");
     unlink("../../../foto_produk/$_GET[namafile]");   
     unlink("../../../foto_produk/small_$_GET[namafile]");
     unlink("../../../foto_produk/medium_$_GET[namafile]");
     if($hasil){
     	$msg->success("Data berhasil hapus!");
		//header('location:../../media.php?p=produk');
		echo "sukses";
     }else{
     	$msg->error("Data gagal hapus!");
     	//header('location:../../media.php?p=produk');
     	echo "gagal";
     }
  }
  else{
     $hasil = mysql_query("DELETE FROM produk WHERE id_produk='$_GET[id]'");
     if($hasil){
     	$msg->success("Data berhasil hapus!");
		//header('location:../../media.php?p=produk');
		echo "sukses";
     }else{
     	$msg->error("Data gagal hapus!");
     	//header('location:../../media.php?p=produk');
     	echo "gagal";
     }
  }
  //header('location:../../index.php?p=produk');


  //mysql_query("DELETE FROM produk WHERE id_produk='$_GET[id]'");
  //header('location:../../media.php?p=produk');
}

// Input produk
elseif ($act=='input'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file; 

  $produk_seo      = seo_title($_POST['nama_produk']);

  // Apabila ada gambar yang diupload
  if (!empty($lokasi_file)){
    if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" && $tipe_file != "image/png"){
		$msg->error("Upload Gagal, Pastikan File yang di Upload bertipe *.JPG atau *.PNG'");
		header('location:../../media.php?p=produk&aksi=tambah');
    }
    else{
	
	if($_POST['nama_produk'] != "" && $_POST['kategori'] != 0 && $_POST['berat'] != "" && $_POST['harga'] != "" && $_POST['diskon'] != "" && $_POST['stok'] != "" && $_POST['deskripsi'] != ""){
		UploadImage($nama_file_unik);

		$hasil = mysql_query("INSERT INTO produk(nama_produk,
		                                produk_seo,
		                                id_kategori,
		                                berat,
		                                harga,
		                                diskon,
		                                stok,
		                                deskripsi,
		                                tgl_masuk,
		                                gambar) 
		                        VALUES('$_POST[nama_produk]',
		                               '$produk_seo',
		                               '$_POST[kategori]',
		                               '$_POST[berat]',
		                               '$_POST[harga]',
		                               '$_POST[diskon]',
		                               '$_POST[stok]',
		                               '$_POST[deskripsi]',
		                               '$tgl_sekarang',
		                               '$nama_file_unik')");
		                            
		  if($hasil){
		  	  $msg->success("Data berhasil disimpan!");
			  header('location:../../media.php?p=produk');
		  }else{
		  	$msg->error("Data gagal disimpan!");
			header('location:../../media.php?p=produk&aksi=tambah');
		  }
	  }else{
	  	  $msg->error("Maaf, Data harus disi lengkap!");
	   	  header('location:../../media.php?p=produk&aksi=tambah');
	  }
	  
  }
  }
  else{
  	if($_POST['nama_produk'] != "" && $_POST['kategori'] != 0 && $_POST['berat'] != "" && $_POST['harga'] != "" && $_POST['diskon'] != "" && $_POST['stok'] != "" && $_POST['deskripsi'] != ""){
		$hasil = mysql_query("INSERT INTO produk(nama_produk,
		                                produk_seo,
		                                id_kategori,
		                                berat,
		                                harga,
		                                diskon,
		                                stok,
		                                deskripsi,
		                                tgl_masuk) 
		                        VALUES('$_POST[nama_produk]',
		                               '$produk_seo',
		                               '$_POST[kategori]',
		                               '$_POST[berat]',                                 
		                               '$_POST[harga]',
		                               '$_POST[harga]',
		                               '$_POST[stok]',
		                               '$_POST[deskripsi]',
		                               '$tgl_sekarang')") or die(mysql_error());
		  if($hasil){
		  	  $msg->success("Data berhasil disimpan!");
			  header('location:../../media.php?p=produk');
		  }else{
			$msg->error("Data gagal disimpan!");
			header('location:../../media.php?p=produk&aksi=tambah');
		  }
	  }else{
	  	  $msg->error("Maaf, Data harus disi lengkap!");
	   	  header('location:../../media.php?p=produk&aksi=tambah');
	  }
  }
}

// Update produk
elseif ($act=='update'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file; 

  $produk_seo      = seo_title($_POST['nama_produk']);

  // Apabila gambar tidak diganti
  if (empty($lokasi_file)){
    $hasil = mysql_query("UPDATE produk SET nama_produk = '$_POST[nama_produk]',
                                   produk_seo  = '$produk_seo', 
                                   id_kategori = '$_POST[kategori]',
                                   berat       = '$_POST[berat]',
                                   harga       = '$_POST[harga]',
                                   diskon      = '$_POST[diskon]',
                                   stok        = '$_POST[stok]',
                                   deskripsi   = '$_POST[deskripsi]'
                             WHERE id_produk   = '$_POST[id]'");
	  if($hasil){
		  $msg->success("Data berhasil diubah!");
		  header('location:../../media.php?p=produk');
	  }else{
		$msg->error("Data gagal disimpan!");
		header('location:../../media.php?p=produk');
	  }
  }
  else{
    if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" && $tipe_file != "image/png"){
		$msg->error("Upload Gagal, Pastikan File yang di Upload bertipe *.JPG atau *.PNG");
		header('location:../../media.php?p=produk&aksi=tambah');
    }
    else{
    UploadImage($nama_file_unik);
    $data=mysql_fetch_array(mysql_query("SELECT gambar FROM produk WHERE id_produk='$_POST[id]'"));
    if ($data['gambar']!=''){
		unlink("../../../foto_produk/$data[gambar]");   
		unlink("../../../foto_produk/small_$data[gambar]");
		unlink("../../../foto_produk/medium_$data[gambar]");
	}
    $hasil = mysql_query("UPDATE produk SET nama_produk = '$_POST[nama_produk]',
                                   produk_seo  = '$produk_seo', 
                                   id_kategori = '$_POST[kategori]',
                                   berat       = '$_POST[berat]',
                                   harga       = '$_POST[harga]',
                                   diskon      = '$_POST[diskon]',
                                   stok        = '$_POST[stok]',
                                   deskripsi   = '$_POST[deskripsi]',
                                   gambar      = '$nama_file_unik'   
                             WHERE id_produk   = '$_POST[id]'");
    if($hasil){
    	  $msg->success("Data berhasil diubah!");
		  header('location:../../media.php?p=produk');
	  }else{
	  	$msg->error("Data gagal disimpan!");
		header('location:../../media.php?p=produk');
	  }
    }
  }
}
}
?>
