<?php
session_start();
require '../FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
include "../../../config/koneksi.php";
include "../../../config/library.php";
include "../../../config/fungsi_thumb.php";
include "../../../config/fungsi_seo.php";

//$module=$_GET['module'];
$act=$_GET['act'];

// Hapus produk
if ($act=='hapus'){
  $data=mysql_fetch_array(mysql_query("SELECT gambar FROM subproduk WHERE id_subproduk='$_GET[id]'"));
  if ($data['gambar']!=''){
  	  unlink("../../../foto_produk/$_GET[namafile]");   
      unlink("../../../foto_produk/small_$_GET[namafile]");
      unlink("../../../foto_produk/medium_$_GET[namafile]");
      $hasil = mysql_query("DELETE FROM subproduk WHERE id_subproduk='$_GET[id]'");
      if($hasil){
       	 $msg->success("Data berhasil hapus!");
		 echo "sukses";
      }else{
     	 $msg->error("Data gagal hapus!");
     	 echo "gagal";
      }
  }else{
  	  $hasil = mysql_query("DELETE FROM subproduk WHERE id_subproduk='$_GET[id]'");
	  if($hasil){
     	 $msg->success("Data berhasil hapus!");
		 echo "sukses";
      }else{
     	 $msg->error("Data gagal hapus!");
     	 echo "gagal";
      }
  }
}

// Input produk
elseif ($act=='input'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file; 

  $produk_seo      = seo_title($_POST['produk']);

  // Apabila ada gambar yang diupload
  if (!empty($lokasi_file) && $_POST['produk'] != "0"){
    UploadImage($nama_file_unik);

    $hasil = mysql_query("INSERT INTO subproduk(id_produk,
                                       gambar) 
                            VALUES('$_POST[produk]',
                                   '$nama_file_unik')");
      if($hasil){
      	  $msg->success("Data berhasil disimpan!");
		  header('location:../../media.php?p=subproduk');
	  }else{
		$msg->error("Data gagal disimpan!");
		header('location:../../media.php?p=subproduk&aksi=tambah');
	  }
  }
  else{
  	  $msg->error("Maaf, Data harus disi lengkap!");
	  header('location:../../media.php?p=subproduk&aksi=tambah');
  }
}

// Update produk
elseif ($act=='update'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file; 

  // Apabila gambar tidak diganti
  if (empty($lokasi_file)){
      $hasil = mysql_query("UPDATE subproduk SET id_produk = '$_POST[produk]'
                             WHERE id_subproduk   = '$_POST[id]'");
      if($hasil){
      	  $msg->success("Data berhasil diubah!");
		  header('location:../../media.php?p=subproduk');
	  }else{
		  $msg->error("Data gagal diubah!");
		  header('location:../../media.php?p=subproduk&aksi=tambah');
	  }
  }
  else{
    UploadImage($nama_file_unik);
    $data=mysql_fetch_array(mysql_query("SELECT gambar FROM subproduk WHERE id_subproduk='$_POST[id]'"));
    if ($data['gambar']!=''){
		unlink("../../../foto_produk/$data[gambar]");   
		unlink("../../../foto_produk/small_$data[gambar]");
		unlink("../../../foto_produk/medium_$data[gambar]");
	}
    $hasil = mysql_query("UPDATE subproduk SET id_produk = '$_POST[produk]',
                                   gambar      = '$nama_file_unik'   
                             WHERE id_subproduk   = '$_POST[id]'");
      if($hasil){
      	  $msg->success("Data berhasil diubah!");
		  header('location:../../media.php?p=subproduk');
	  }else{
		  $msg->error("Data gagal diubah!");
		  header('location:../../media.php?p=subproduk&aksi=tambah');
	  }
  }
}
?>
