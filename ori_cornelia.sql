-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2017 at 12:04 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cornelia`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`username`, `password`, `nama_lengkap`, `email`, `no_telp`, `level`, `blokir`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'redaksi@bukulokomedia.com', '08238923848', 'admin', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id_bank` int(5) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `no_rekening` varchar(100) NOT NULL,
  `pemilik` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id_bank`, `nama_bank`, `no_rekening`, `pemilik`, `gambar`) VALUES
(1, 'BRI', '126.00.05244.71.9', 'Niken Sulanjari', 'bri.png'),
(5, 'BCA', '123.456.332.223', 'Faiza Sidqi', 'bca.png'),
(6, 'DANAMON', '3356.12233.123', 'Ilamsyah', 'danamon.png'),
(7, 'BNI', '999.1234.4456', 'Fathan', 'bni.png');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(5) NOT NULL,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id_banner`, `judul`, `url`, `gambar`, `tgl_posting`) VALUES
(15, 'Contoh Iklan', 'http://www.iklan.com', 'contohiklan.jpg', '2011-03-13'),
(16, 'I Love Indonesia', 'http://iloveindonesia.com', 'ilove_indonesia.jpg', '2011-03-29'),
(17, 'jardiknas', 'jardiknas.co.id', 'jardiknas.jpg', '2013-11-18');

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `id_download` int(5) NOT NULL,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `nama_file` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `hits` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `download`
--

INSERT INTO `download` (`id_download`, `judul`, `nama_file`, `tgl_posting`, `hits`) VALUES
(10, 'Katalog 001', 'test.jpg', '2011-01-31', 2);

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `id_header` int(5) NOT NULL,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `tgl_posting` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`id_header`, `judul`, `url`, `gambar`, `deskripsi`, `tgl_posting`) VALUES
(21, 'Header1', '', 'banner1.jpg', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at nunc a magna tincidunt placerat. In hac habitasse platea dictumst. Donec in tellus libero. Lorem ipsum dolor sit amet, consect', '2011-03-29'),
(22, 'Header2', '', 'banner2.jpg', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at nunc a magna tincidunt placerat. In hac habitasse platea dictumst. Donec in tellus libero. Lorem ipsum dolor sit amet, consect', '2011-03-29'),
(23, 'Header3', '', 'banner1.jpg', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at nunc a magna tincidunt placerat. In hac habitasse platea dictumst. Donec in tellus libero. Lorem ipsum dolor sit amet, consect', '2011-03-29'),
(24, 'Header4', '', 'banner2.jpg', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at nunc a magna tincidunt placerat. In hac habitasse platea dictumst. Donec in tellus libero. Lorem ipsum dolor sit amet, consect', '2011-03-29');

-- --------------------------------------------------------

--
-- Table structure for table `hubungi`
--

CREATE TABLE `hubungi` (
  `id_hubungi` int(5) NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `subjek` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pesan` text COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `hubungi`
--

INSERT INTO `hubungi` (`id_hubungi`, `nama`, `email`, `subjek`, `pesan`, `tanggal`) VALUES
(1, 'cor', 'cor', 'cor', 'cor test', '2017-02-04');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `kategori_seo` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `kategori_seo`) VALUES
(25, 'Sun Glass', 'sun-glass');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(5) NOT NULL,
  `id_produk` int(5) NOT NULL,
  `nama_komentar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `isi_komentar` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl` date NOT NULL,
  `jam_komentar` time NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_produk`, `nama_komentar`, `url`, `isi_komentar`, `tgl`, `jam_komentar`, `aktif`) VALUES
(77, 54, 'Rizal Faizal', 'teraskreasi.com', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut diam quis dolor mollis tristique. Suspendisse vestibulum convallis felis vitae facilisis. Praesent eu nisi vestibulum erat lacinia sollicitudin. Cras nec risus dolor, ut tristique neque. Donec mauris sapien, pellentesque at porta id, varius eu tellus. Maecenas nulla felis, commodo et adipiscing vel, accumsan eget augue. Morbi volutpat iaculis molestie. ', '2011-02-23', '23:39:46', 'Y'),
(79, 561, 'ilamsyah', 'ilamsyah.ac.id', ' In id adipiscing diam. Sed lobortis dui ut odio tempor blandit. Suspendisse scelerisque mi nec nunc gravida quis mollis lacus dignissim. Cras nec risus dolor, ut tristique neque. Donec mauris sapien, pellentesque at porta id, varius eu tellus. Maecenas nulla felis, commodo et adipiscing vel, accumsan eget augue morbi volutpat. ', '2013-11-12', '11:46:27', 'Y'),
(82, 561, 'faiza', 'jardiknas.co.id', ' salam  &#039;alaykum   ', '2013-12-01', '11:11:51', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id_kota` int(3) NOT NULL,
  `id_perusahaan` int(10) NOT NULL,
  `nama_kota` varchar(100) NOT NULL,
  `ongkos_kirim` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `id_perusahaan`, `nama_kota`, `ongkos_kirim`) VALUES
(5, 5, 'Jakarta', 15000),
(6, 6, 'Bandung', 15000),
(7, 5, 'Surabaya', 13000),
(8, 5, 'Semarang', 17500),
(9, 6, 'Medan', 20000),
(10, 6, 'Aceh', 25000),
(11, 5, 'Banjarmasin', 18500),
(12, 10, 'Lain - Lain', 25000);

-- --------------------------------------------------------

--
-- Table structure for table `kustomer`
--

CREATE TABLE `kustomer` (
  `id_kustomer` int(5) NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `telpon` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id_kota` int(5) NOT NULL,
  `aktif` enum('N','Y') DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kustomer`
--

INSERT INTO `kustomer` (`id_kustomer`, `password`, `nama_lengkap`, `alamat`, `email`, `telpon`, `id_kota`, `aktif`) VALUES
(1, 'e10adc3949ba59abbe56e057f20f883e', 'Lukmanul Hakim', 'Jl. Prof. Dr. Soepomo No. 178, Tebet, Jakarta Timur 17280', 'algosigma@gmail.com', '081804396000', 1, 'Y'),
(2, 'bf91184832ff2e9b92e94ef61ad52e53', 'ilamsyah', 'Kp. sawah dalam no 56', 'ilamsyah@yahoo.com', '02197604687', 9, 'Y'),
(3, '0e987a3e88814566657aed9e53e38328', 'ilamsyah', 'Kp. sawah dalam no 67 kel. panunggangan utara', 'ilamsyah@ymail.com', '02197604687', 1, 'N'),
(4, '5f4dcc3b5aa765d61d8327deb882cf99', 'faiza', 'Kp. sawah dalam no 67', 'faiza_sidqi@gmail.com', '087771063961', 9, 'Y'),
(8, '5f4dcc3b5aa765d61d8327deb882cf99', 'Lyra virna', 'BSD City', 'lyra82@gmail.com', '087771063961', 5, 'Y'),
(10, 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'jl. penjara no 3, kota pontianak, kode post 76948', 'user@mail.com', '089764758475', 10, 'Y'),
(11, 'cebd65946444e5cd3e861a2dbf69e221', 'cor', 'jl. ayani', 'cor@mail.com', '086655678676', 5, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `mainmenu`
--

CREATE TABLE `mainmenu` (
  `id_main` int(5) NOT NULL,
  `nama_menu` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `link` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mainmenu`
--

INSERT INTO `mainmenu` (`id_main`, `nama_menu`, `link`, `aktif`) VALUES
(10, 'BERANDA', 'index.php', 'Y'),
(11, 'PROFIL ', 'profil-kami.html', 'Y'),
(12, 'PRODUK', 'semua-produk.html', 'Y'),
(13, 'KERANJANG BELANJA', 'keranjang-belanja.html', 'Y'),
(14, 'CARA PEMBELIAN', 'cara-pembelian.html', 'Y'),
(15, 'DOWNLOAD KATALOG', 'semua-download.html', 'Y'),
(16, 'HUBUNGI KAMI', 'hubungi-kami.html', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `modul`
--

CREATE TABLE `modul` (
  `id_modul` int(5) NOT NULL,
  `nama_modul` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `link` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `static_content` text COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `status` enum('user','admin') COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  `urutan` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `modul`
--

INSERT INTO `modul` (`id_modul`, `nama_modul`, `link`, `static_content`, `gambar`, `status`, `aktif`, `urutan`) VALUES
(18, 'Tambah Produk', '?module=produk', '', '', 'admin', 'Y', 5),
(42, 'Lihat Order Masuk', '?module=order', '', '', 'admin', 'Y', 8),
(10, 'Manajemen Modul', '?module=modul', '', '', 'admin', 'Y', 19),
(31, 'Tambah Kategori Produk', '?module=kategori', '', '', 'admin', 'Y', 4),
(43, 'Edit Profil', '?module=profil', '<p class="MsoNormal">', '12sfhijau.jpg', 'admin', 'Y', 7),
(44, 'Lihat Pesan Masuk', '?module=hubungi', '<!--[if gte mso 9]><xml>', '', 'admin', 'Y', 9),
(45, ' Edit Cara Pembelian', '?module=carabeli', '<ol><span class="center_content2"><li>Klik pada tombol&nbsp;<span style="font-weight: bold">Beli</span> pada produk yang ingin Anda pesan.</li>\r\n	<li>Produk yang Anda pesan akan masuk ke dalam <span style="font-weight: bold">Keranjang Belanja</span>. Anda dapat melakukan perubahan jumlah produk yang diinginkan dengan mengganti angka di kolom <span style="font-weight: bold">Jumlah</span>, kemudian klik tombol <span style="font-weight: bold">Update</span>. Sedangkan untuk menghapus sebuah produk dari Keranjang Belanja, klik tombol Kali yang berada di kolom paling kanan.</li>\r\n	<li>Jika sudah selesai, klik tombol <span style="font-weight: bold">Selesai Belanja</span>, maka akan tampil form untuk pengisian data kustomer/pembeli.</li>\r\n	<li>Setelah data pembeli selesai diisikan, klik tombol <span style="font-weight: bold">Proses</span>,\r\n maka akan tampil data pembeli beserta produk yang dipesannya (jika \r\ndiperlukan catat nomor ordernya). Dan juga ada total pembayaran serta \r\nnomor rekening pembayaran.</li>\r\n	<li>Apabila telah melakukan pembayaran, maka produk/barang akan segera kami kirimkan. </li></span></ol><w:worddocument></w:worddocument>', 'gedung.jpg', 'admin', 'Y', 10),
(47, 'Edit Link Terkait', '?module=banner', '<w:View>Normal</w:View>', '', 'user', 'Y', 13),
(48, 'Edit Ongkos Kirim', '?module=ongkoskirim', '<w:Zoom>0</w:Zoom>', '', 'user', 'Y', 11),
(49, 'Ganti Password', '?module=password', '<w:Compatibility>', '', 'user', 'Y', 1),
(53, 'User Yahoo Messenger', '?module=ym', '<w:BreakWrappedTables/>', '', 'user', 'Y', 15),
(52, 'Lihat Laporan Transaksi', '?module=laporan', '<w:SnapToGridInCell/>', '', 'user', 'Y', 14),
(66, 'Edit Jasa Pengiriman', '?module=jasapengiriman', '<w:WrapTextWithPunct/>', 'hai.jpg', 'user', 'Y', 12),
(73, '', '', 'margin:0cm;', '', 'user', 'Y', 0),
(74, '', '', 'margin-bottom:.0001pt;', '', 'user', 'Y', 0),
(75, '', '', 'mso-pagination:widow-orphan;', '', 'user', 'Y', 0),
(76, '', '', 'font-size:12.0pt;', '', 'user', 'Y', 0),
(77, '', '', 'font-family:"Times New Roman";', '', 'user', 'Y', 0),
(78, '', '', 'mso-fareast-font-family:"Times New Roman";}', '', 'user', 'Y', 0),
(79, '', '', '@page Section1', '', 'user', 'Y', 0),
(80, '', '', '{size:612.0pt 792.0pt;', '', 'user', 'Y', 0),
(81, '', '', 'margin:72.0pt 90.0pt 72.0pt 90.0pt;', '', 'user', 'Y', 0),
(82, '', '', 'mso-header-margin:35.4pt;', '', 'user', 'Y', 0),
(83, '', '', 'mso-footer-margin:35.4pt;', '', 'user', 'Y', 0),
(84, '', '', 'mso-paper-source:0;}', '', 'user', 'Y', 0),
(85, '', '', 'div.Section1', '', 'user', 'Y', 0),
(86, '', '', '{page:Section1;}', '', 'user', 'Y', 0),
(87, '', '', '-->', '', 'user', 'Y', 0),
(88, '', '', '<!--[if gte mso 10]>', '', 'user', 'Y', 0),
(89, '', '', '<style>', '', 'user', 'Y', 0),
(90, '', '', '/* Style Definitions */', '', 'user', 'Y', 0),
(91, '', '', 'table.MsoNormalTable', '', 'user', 'Y', 0),
(92, '', '', '{mso-style-name:"Table Normal";', '', 'user', 'Y', 0),
(93, '', '', 'mso-tstyle-rowband-size:0;', '', 'user', 'Y', 0),
(94, '', '', 'mso-tstyle-colband-size:0;', '', 'user', 'Y', 0),
(95, '', '', 'mso-style-noshow:yes;', '', 'user', 'Y', 0),
(96, '', '', 'mso-style-parent:"";', '', 'user', 'Y', 0),
(97, '', '', 'mso-padding-alt:0cm 5.4pt 0cm 5.4pt;', '', 'user', 'Y', 0),
(98, '', '', 'mso-para-margin:0cm;', '', 'user', 'Y', 0),
(99, '', '', 'mso-para-margin-bottom:.0001pt;', '', 'user', 'Y', 0),
(100, '', '', 'mso-pagination:widow-orphan;', '', 'user', 'Y', 0),
(101, '', '', 'font-size:10.0pt;', '', 'user', 'Y', 0),
(102, '', '', 'font-family:"Times New Roman";}', '', 'user', 'Y', 0),
(103, '', '', '</style>', '', 'user', 'Y', 0),
(104, '', '', '<![endif]--><font size="2">ArtFashion adalah toko fashion online, yang menyediakan segala kebutuhan fashion anda. ArtFashion ingin memberikan kemudahan kepada para calon pembeli, cara santai, mudah dan hemat dalam berbelanja fashion berkualias dengan harga yang terjangkau.', '', 'user', 'Y', 0),
(105, '', '', '</font>', '', 'user', 'Y', 0),
(106, '', '', '</p>', '', 'user', 'Y', 0),
(107, '', '', '<p class="MsoNormal">', '', 'user', 'Y', 0),
(108, '', '', '<font size="2">Karena itulah website ini dibuat sedemikian sederhananya sehingga diharapkan dapat membantu para pengunjung untuk dapat menelusuri produk-produk yang ditawarkan secara lebih mudah.<br />', '', 'user', 'Y', 0),
(109, '', '', '</font>', '', 'user', 'Y', 0),
(110, '', '', '</p>', '', 'user', 'Y', 0),
(111, '', '', '<p class="MsoNormal">', '', 'user', 'Y', 0),
(112, '', '', '<font size="2">Selain melayani pesanan produk-produk yang ada di toko online, kami menerima pembuatan/pemesanan fashion sesuai design/pola&nbsp; yang anda inginkan.<br />', '', 'user', 'Y', 0),
(113, '', '', '</font>', '', 'user', 'Y', 0),
(114, '', '', '</p>', '', 'user', 'Y', 0),
(115, '', '', '<p class="MsoNormal">', '', 'user', 'Y', 0),
(116, '', '', '<font size="2">Akhirnya, kami mengucapkan terima kasih atas kunjungan anda di ArtFashion.', '', 'user', 'Y', 0),
(117, '', '', '</font>', '', 'user', 'Y', 0),
(118, '', '', '</p>', '', 'user', 'Y', 0),
(57, 'Edit Rekening Bank', '?module=bank', '<w:UseAsianBreakRules/>', '', 'user', 'Y', 16),
(58, 'Edit Selamat Datang', '?module=welcome', '</w:Compatibility>', 'gedung.jpg', 'user', 'Y', 6),
(71, '', '', 'p.MsoNormal, li.MsoNormal, div.MsoNormal', '', 'user', 'Y', 0),
(72, '', '', '{mso-style-parent:"";', '', 'user', 'Y', 0),
(59, 'Ganti Header', '?module=header', '<w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>', '', 'user', 'Y', 18),
(61, 'Edit Menu Utama', '?module=menuutama', '</w:WordDocument>', '', 'user', 'Y', 2),
(62, 'Edit Sub Menu', '?module=submenu', '</xml><![endif]-->', '', 'user', 'Y', 3),
(63, 'Edit Download Katalog', '?module=download', '<!--', '', 'user', 'Y', 17),
(70, 'Edit Jasa Pengiriman', '?module=jasapengiriman', '/* Style Definitions */', '', 'user', 'Y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id_orders` int(5) NOT NULL,
  `status_order` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT 'Baru',
  `tgl_order` date NOT NULL,
  `jam_order` time NOT NULL,
  `id_kustomer` int(5) NOT NULL,
  `metode` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `stattrans` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `harustrans` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `statterima` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id_orders`, `status_order`, `tgl_order`, `jam_order`, `id_kustomer`, `metode`, `stattrans`, `harustrans`, `statterima`) VALUES
(1, 'Lunas/Terkirim', '2017-02-04', '02:19:54', 10, 'Transfer Bank', 'Sudah Transfer', '172.200', 'Sudah Terima'),
(2, 'Baru', '2017-02-05', '17:05:16', 10, 'Transfer Bank', 'Belum Transfer', '344.400', '');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

CREATE TABLE `orders_detail` (
  `id_orders` int(5) NOT NULL,
  `id_produk` int(5) NOT NULL,
  `jumlah` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`id_orders`, `id_produk`, `jumlah`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders_temp`
--

CREATE TABLE `orders_temp` (
  `id_orders_temp` int(5) NOT NULL,
  `id_produk` int(5) NOT NULL,
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `jumlah` int(5) NOT NULL,
  `tgl_order_temp` date NOT NULL,
  `jam_order_temp` time NOT NULL,
  `stok_temp` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `orders_temp`
--

INSERT INTO `orders_temp` (`id_orders_temp`, `id_produk`, `id_session`, `jumlah`, `tgl_order_temp`, `jam_order_temp`, `stok_temp`) VALUES
(13, 1, 'ko4eb3dns6csphn25ik5v03173', 1, '2017-02-04', '11:45:26', 19),
(12, 1, 'sb8f3jac48f8h1an5q2jfd2bi4', 1, '2017-02-04', '11:44:31', 19),
(8, 2, 'gahi1b5mtqg00n57s97kajr8g7', 1, '2017-02-04', '02:12:16', 12),
(11, 1, 'fi4u25p34o0cef3mh424bgab32', 1, '2017-02-04', '11:37:00', 19);

-- --------------------------------------------------------

--
-- Table structure for table `poling`
--

CREATE TABLE `poling` (
  `id_poling` int(5) NOT NULL,
  `pilihan` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `status` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `rating` int(5) NOT NULL DEFAULT '0',
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `poling`
--

INSERT INTO `poling` (`id_poling`, `pilihan`, `status`, `rating`, `aktif`) VALUES
(1, 'Bagus', 'Jawaban', 27, 'Y'),
(2, 'Lumayan', 'Jawaban', 80, 'Y'),
(3, 'Tidak', 'Jawaban', 21, 'Y'),
(8, 'Bagaimana tampilan web ini?', 'Pertanyaan', 0, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(5) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `produk_seo` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(20) NOT NULL,
  `stok` int(5) NOT NULL,
  `berat` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `tgl_masuk` date NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `dibeli` int(5) NOT NULL DEFAULT '1',
  `diskon` int(5) NOT NULL DEFAULT '0',
  `status` varchar(10) DEFAULT NULL,
  `review` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_kategori`, `nama_produk`, `produk_seo`, `deskripsi`, `harga`, `stok`, `berat`, `tgl_masuk`, `gambar`, `dibeli`, `diskon`, `status`, `review`) VALUES
(1, 25, 'Kacamata 1', 'kacamata-1', 'fullset : <br>\r\nlap <br>\r\nsoftcase <br>\r\nhardcase <br>\r\nbox <br>\r\nbonus semprotan pembersih lensa ', 160000, 19, '1.00', '2017-01-31', '66kacamata oakley daytona aviator motogp full black polarized lens.jpg', 2, 8, NULL, NULL),
(2, 25, 'Kacamata 2', 'kacamata-2', 'HARAP KETERANGAN WARNA DI ISI APABILA ORDER!<br><br>DISKRIPSI PRODUK<br>FULLSET<br>BOX SEPERTI DI GAMBAR<br>MODEL TRENDY<br><br>GAMBAR ADALAH GAMBAR ASLI YG KAMI TARUH SO APA YG ANDA LIAT ITULAH APA YG ANDA DAPAT senyum<br><br>BACA KEBIJAKAN PENGEMBALIAN PRODUK SEBLOM ATC!!<br><br>SELAMAT BERBELANJA ^^<br>THANKS SUDAH PERCAYA &amp; MEMILIH TOKO KAMI^^ <br>', 160000, 12, '1.00', '2017-01-31', '63KACAMATA WANITA RAYBAN 2630 SUPER FULLSET9.jpg', 1, 0, NULL, NULL),
(3, 25, 'Kacamata 3', 'kacamata-3', 'HARAP KETERANGAN WARNA DI ISI APABILA ORDER!<br><br>DISKRIPSI PRODUK<br>FULLSET<br>BOX SEPERTI DI GAMBAR<br>MODEL TRENDY<br><br>GAMBAR ADALAH GAMBAR ASLI YG KAMI TARUH SO APA YG ANDA LIAT ITULAH APA YG ANDA DAPAT senyum<br><br>BACA KEBIJAKAN PENGEMBALIAN PRODUK SEBLOM ATC!!<br><br>SELAMAT BERBELANJA ^^<br>THANKS SUDAH PERCAYA &amp; MEMILIH TOKO KAMI^^', 200000, 15, '1.00', '2017-01-31', '90KACAMATA WANITA VERSACE 792 SUPER FULLSET9.jpg', 1, 0, NULL, NULL),
(4, 25, 'Kacamata 4', 'kacamata-4', 'kelengkapan seperti difoto FULLSET (BUKAN BOX SERITING ) *FREE CLEANER', 196000, 20, '1.00', '2017-01-31', '13KACAMATA WANITA DIOR3.jpg', 1, 0, NULL, NULL),
(5, 25, 'Kacamata 5', 'kacamata-5', 'Kualitas : Super FULL SET BOX ORI<br>WARNA PINK<br><br>Include :<br>- Sertifikat dior<br>- Box dior<br>- Hard Case dior<br>- Lap<br>- FreE Cairan Lensa <br>', 145000, 17, '1.00', '2017-01-31', '78frame kacamata wanita sunglas DIOR 1297.jpg', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sekilasinfo`
--

CREATE TABLE `sekilasinfo` (
  `id_sekilas` int(5) NOT NULL,
  `info` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `sekilasinfo`
--

INSERT INTO `sekilasinfo` (`id_sekilas`, `info`, `tgl_posting`, `gambar`) VALUES
(1, 'Anak yang mengalami gangguan tidur, cenderung memakai obat2an dan alkohol berlebih saat dewasa.', '2010-04-11', 'news5.jpg'),
(2, 'WHO merilis, 30 persen anak-anak di dunia kecanduan menonton televisi dan bermain komputer.', '2010-04-11', 'news4.jpg'),
(3, 'Menurut peneliti di Detroit, orang yang selalu tersenyum lebar cenderung hidup lebih lama.', '2010-04-11', 'news3.jpg'),
(4, 'Menurut United Stated Trade Representatives, 25% obat yang beredar di Indonesia adalah palsu.', '2010-04-11', 'news2.jpg'),
(5, 'Presiden AS Barack Obama memesan Nasi Goreng di restoran Bali langsung dari Amerika', '2010-04-11', 'news1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `shop_pengiriman`
--

CREATE TABLE `shop_pengiriman` (
  `id_perusahaan` int(10) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_pengiriman`
--

INSERT INTO `shop_pengiriman` (`id_perusahaan`, `nama_perusahaan`, `alias`, `gambar`) VALUES
(6, 'JNE', 'Jasa Network Enterprise', 'jne.jpg'),
(5, 'TIKI', 'Titipan Kilat', 'tiki.jpg'),
(7, 'POS EKSPRESS', 'POS EKSPRESS', 'pos.jpg'),
(10, 'Lain - lain', 'Lain - lain', '89other courir.png');

-- --------------------------------------------------------

--
-- Table structure for table `statistik`
--

CREATE TABLE `statistik` (
  `ip` varchar(20) NOT NULL DEFAULT '',
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL DEFAULT '1',
  `online` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistik`
--

INSERT INTO `statistik` (`ip`, `tanggal`, `hits`, `online`) VALUES
('127.0.0.1', '2011-01-23', 406, '1295797934'),
('127.0.0.1', '2011-01-22', 199, '1295712739'),
('127.0.0.1', '2011-01-20', 18, '1295484485'),
('127.0.0.1', '2011-01-19', 10, '1295452438'),
('127.0.0.1', '2011-01-25', 2, '1295961873'),
('127.0.0.1', '2011-01-26', 4, '1296050267'),
('127.0.0.1', '2011-01-27', 7, '1296110326'),
('127.0.0.1', '2011-01-28', 7, '1296233314'),
('127.0.0.1', '2011-01-29', 574, '1296320383'),
('127.0.0.1', '2011-01-30', 290, '1296393287'),
('127.0.0.1', '2011-01-31', 133, '1296493024'),
('127.0.0.1', '2011-02-01', 79, '1296521132'),
('110.138.43.143', '2011-02-01', 31, '1296540211'),
('66.249.71.118', '2011-02-01', 1, '1296528448'),
('67.195.115.24', '2011-02-01', 6, '1296538036'),
('125.161.211.231', '2011-02-01', 1, '1296529398'),
('222.124.98.187', '2011-02-01', 3, '1296531520'),
('66.249.71.77', '2011-02-01', 1, '1296532249'),
('66.249.71.20', '2011-02-01', 1, '1296534199'),
('117.20.62.233', '2011-02-01', 13, '1296537677'),
('110.137.200.121', '2011-02-01', 24, '1296540049'),
('127.0.0.1', '2011-02-16', 179, '1297875502'),
('127.0.0.1', '2011-02-17', 301, '1297961988'),
('127.0.0.1', '2011-02-18', 54, '1297990124'),
('127.0.0.1', '2011-02-22', 118, '1298393910'),
('127.0.0.1', '2011-02-23', 77, '1298479971'),
('127.0.0.1', '2011-02-24', 1, '1298510525'),
('127.0.0.1', '2011-03-13', 225, '1300027455'),
('127.0.0.1', '2011-03-14', 44, '1300115678'),
('127.0.0.1', '2011-03-15', 121, '1300195187'),
('127.0.0.1', '2011-03-16', 116, '1300292361'),
('127.0.0.1', '2011-03-17', 4, '1300331607'),
('127.0.0.1', '2011-03-18', 52, '1300422211'),
('127.0.0.1', '2011-03-27', 75, '1301234016'),
('127.0.0.1', '2011-03-28', 16, '1301307056'),
('127.0.0.1', '2011-03-29', 77, '1301409884'),
('127.0.0.1', '2012-10-11', 8, '1349939081'),
('127.0.0.1', '2012-10-18', 13, '1350574484'),
('127.0.0.1', '2012-10-21', 1, '1350796772'),
('127.0.0.1', '2012-10-22', 1, '1350878719'),
('127.0.0.1', '2012-10-23', 6, '1350984577'),
('127.0.0.1', '2012-10-25', 1, '1351146419'),
('127.0.0.1', '2012-10-28', 2, '1351441921'),
('127.0.0.1', '2012-11-02', 1, '1351875551'),
('127.0.0.1', '2012-11-03', 5, '1351876314'),
('127.0.0.1', '2012-12-10', 13, '1355156413'),
('127.0.0.1', '2012-12-11', 1, '1355173951'),
('127.0.0.1', '2012-12-14', 2, '1355431434'),
('127.0.0.1', '2013-01-08', 108, '1357627283'),
('127.0.0.1', '2013-01-11', 1, '1357879761'),
('127.0.0.1', '2013-01-13', 16, '1358090797'),
('127.0.0.1', '2013-02-23', 1, '1361582818'),
('127.0.0.1', '2013-03-10', 1, '1362920556'),
('127.0.0.1', '2013-03-18', 4, '1363581022'),
('127.0.0.1', '2013-03-19', 5, '1363666788'),
('127.0.0.1', '2013-04-26', 1, '1366985043'),
('127.0.0.1', '2013-04-29', 3, '1367201227'),
('127.0.0.1', '2013-05-07', 2, '1367943522'),
('127.0.0.1', '2013-05-15', 1, '1368596741'),
('127.0.0.1', '2013-05-19', 2, '1368974386'),
('127.0.0.1', '2013-06-05', 2, '1370438052'),
('127.0.0.1', '2013-06-10', 4, '1370833789'),
('127.0.0.1', '2013-06-13', 3, '1371094792'),
('127.0.0.1', '2013-06-16', 2, '1371399095'),
('127.0.0.1', '2013-07-28', 40, '1375030405'),
('127.0.0.1', '2013-07-29', 1, '1375031690'),
('127.0.0.1', '2013-10-04', 1, '1380895135'),
('127.0.0.1', '2013-10-30', 1, '1383069939'),
('127.0.0.1', '2013-11-29', 237, '1385709004'),
('127.0.0.1', '2013-11-30', 17, '1385783344'),
('127.0.0.1', '2013-12-01', 102, '1385882699'),
('127.0.0.1', '2013-12-02', 153, '1385999681'),
('127.0.0.1', '2013-12-03', 53, '1386027048'),
('::1', '2013-12-03', 54, '1386056497'),
('::1', '2013-12-04', 23, '1386132745'),
('127.0.0.1', '2013-12-04', 1, '1386125365'),
('::1', '2013-12-05', 68, '1386262659'),
('::1', '2013-12-06', 187, '1386301411'),
('::1', '2013-12-07', 84, '1386432534'),
('::1', '2013-12-09', 8, '1386607106'),
('::1', '2013-12-10', 4, '1386611074'),
('127.0.0.1', '2015-12-16', 267, '1450285161'),
('127.0.0.1', '2015-12-17', 217, '1450370636'),
('127.0.0.1', '2015-12-18', 590, '1450457955'),
('127.0.0.1', '2015-12-19', 76, '1450461950'),
('127.0.0.1', '2015-12-21', 32, '1450705771'),
('127.0.0.1', '2017-01-26', 400, '1485449986'),
('127.0.0.1', '2017-01-27', 172, '1485533903'),
('127.0.0.1', '2017-01-28', 240, '1485622703'),
('127.0.0.1', '2017-01-29', 194, '1485708971'),
('127.0.0.1', '2017-01-30', 458, '1485795579'),
('127.0.0.1', '2017-01-31', 224, '1485864252'),
('127.0.0.1', '2017-02-04', 31, '1486154775');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `id_sub` int(5) NOT NULL,
  `nama_sub` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `link_sub` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `id_main` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`id_sub`, `nama_sub`, `link_sub`, `id_main`) VALUES
(26, 'KOLEKSI BAJU', 'kategori-17-koleksi-baju.html', 12),
(25, 'KOLEKSI BAJU', 'kategori-17-koleksi-baju.html', 0),
(23, 'CELANA GAUL', 'kategori-15-celana-gaul.html', 12),
(24, 'RAGAM TOPI', 'kategori-18-ragam-topi.html', 12),
(20, 'LIHAT KERANJANG', 'keranjang-belanja.html', 13),
(21, 'SELESAI BELANJA', 'selesai-belanja.html', 13),
(27, 'JAKET GAYA', 'kategori-16-jaket-gaya.html', 12),
(28, 'ANEKA KAOS', 'kategori-14-aneka-kaos.html', 12);

-- --------------------------------------------------------

--
-- Table structure for table `subproduk`
--

CREATE TABLE `subproduk` (
  `id_subproduk` int(5) NOT NULL,
  `id_produk` int(5) NOT NULL,
  `gambar` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subproduk`
--

INSERT INTO `subproduk` (`id_subproduk`, `id_produk`, `gambar`) VALUES
(1, 1, '43kacamata oakley daytona aviator motogp full black polarized lens.jpg'),
(2, 2, '38KACAMATA WANITA RAYBAN 2630 SUPER FULLSET9.jpg'),
(3, 3, '32KACAMATA WANITA VERSACE 792 SUPER FULLSET9.jpg'),
(4, 4, '20KACAMATA WANITA DIOR3.jpg'),
(5, 5, '27frame kacamata wanita sunglas DIOR 1297.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id_tag` int(5) NOT NULL,
  `nama_tag` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tag_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `count` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id_tag`, `nama_tag`, `tag_seo`, `count`) VALUES
(1, 'Palestina', 'palestina', 7),
(2, 'Gaza', 'gaza', 11),
(9, 'Tenis', 'tenis', 5),
(10, 'Sepakbola', 'sepakbola', 7),
(8, 'Laskar Pelangi', 'laskar-pelangi', 2),
(11, 'Amerika', 'amerika', 18),
(12, 'George Bush', 'george-bush', 3),
(13, 'Browser', 'browser', 9),
(14, 'Google', 'google', 3),
(15, 'Israel', 'israel', 5),
(16, 'Komputer', 'komputer', 24),
(17, 'Film', 'film', 9),
(19, 'Mobil', 'mobil', 0),
(21, 'Gayus', 'gayus', 2);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_trans` int(11) NOT NULL,
  `id_kustomer` int(11) NOT NULL,
  `id_orders` int(11) NOT NULL,
  `tgltrans` varchar(100) NOT NULL,
  `bank` varchar(20) NOT NULL,
  `norek` varchar(20) NOT NULL,
  `atasnama` varchar(100) NOT NULL,
  `kebank` varchar(20) NOT NULL,
  `jumlahtransfer` int(11) NOT NULL,
  `jumlahharustransfer` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `imgresi` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_trans`, `id_kustomer`, `id_orders`, `tgltrans`, `bank`, `norek`, `atasnama`, `kebank`, `jumlahtransfer`, `jumlahharustransfer`, `keterangan`, `imgresi`) VALUES
(18, 10, 1, '1/1/2012', 'bca', '111', 'kkk', '1', 222, 0, 'test', 'resi-71a02f67-8448-433f-8ea9-9b6cbb3287be.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password1` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktivasi` int(6) NOT NULL DEFAULT '0',
  `cek_aktivasi` int(6) NOT NULL DEFAULT '0',
  `no_telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id_kota` int(2) NOT NULL,
  `level` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password1`, `password`, `nama_lengkap`, `alamat`, `email`, `aktivasi`, `cek_aktivasi`, `no_telp`, `id_kota`, `level`, `blokir`, `id_session`) VALUES
('', '', '5f4dcc3b5aa765d61d8327deb882cf99', 'ilamsyah', 'tangerang', 'ilamsyah011@gmail.com', 0, 0, '0219298389', 10, 'user', 'N', '');

-- --------------------------------------------------------

--
-- Table structure for table `ym`
--

CREATE TABLE `ym` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `ym`
--

INSERT INTO `ym` (`id`, `nama`, `username`) VALUES
(1, 'ilamsyah', 'ilamsyah');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id_download`);

--
-- Indexes for table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`id_header`);

--
-- Indexes for table `hubungi`
--
ALTER TABLE `hubungi`
  ADD PRIMARY KEY (`id_hubungi`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `kustomer`
--
ALTER TABLE `kustomer`
  ADD PRIMARY KEY (`id_kustomer`);

--
-- Indexes for table `mainmenu`
--
ALTER TABLE `mainmenu`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`id_modul`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_orders`);

--
-- Indexes for table `orders_temp`
--
ALTER TABLE `orders_temp`
  ADD PRIMARY KEY (`id_orders_temp`);

--
-- Indexes for table `poling`
--
ALTER TABLE `poling`
  ADD PRIMARY KEY (`id_poling`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `sekilasinfo`
--
ALTER TABLE `sekilasinfo`
  ADD PRIMARY KEY (`id_sekilas`);

--
-- Indexes for table `shop_pengiriman`
--
ALTER TABLE `shop_pengiriman`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `subproduk`
--
ALTER TABLE `subproduk`
  ADD PRIMARY KEY (`id_subproduk`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `ym`
--
ALTER TABLE `ym`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id_bank` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id_download` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `header`
--
ALTER TABLE `header`
  MODIFY `id_header` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `hubungi`
--
ALTER TABLE `hubungi`
  MODIFY `id_hubungi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `kustomer`
--
ALTER TABLE `kustomer`
  MODIFY `id_kustomer` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mainmenu`
--
ALTER TABLE `mainmenu`
  MODIFY `id_main` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `modul`
--
ALTER TABLE `modul`
  MODIFY `id_modul` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id_orders` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders_temp`
--
ALTER TABLE `orders_temp`
  MODIFY `id_orders_temp` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `poling`
--
ALTER TABLE `poling`
  MODIFY `id_poling` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sekilasinfo`
--
ALTER TABLE `sekilasinfo`
  MODIFY `id_sekilas` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `shop_pengiriman`
--
ALTER TABLE `shop_pengiriman`
  MODIFY `id_perusahaan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `id_sub` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `subproduk`
--
ALTER TABLE `subproduk`
  MODIFY `id_subproduk` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id_tag` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_trans` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `ym`
--
ALTER TABLE `ym`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
