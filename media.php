<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
	error_reporting(0);
	session_start();
	include "config/koneksi.php";
	include "config/fungsi_indotgl.php";
	include "config/pagingproduk.php";
	include "config/fungsi_combobox.php";
	include "config/library.php";
	include "config/fungsi_autolink.php";
	include "config/fungsi_rupiah.php";

	if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
		$user="Pengunjung";
	}else{
		$user="$_SESSION[namalengkap]";
	}

 ?>

<!DOCTYPE HTML>
<html>
<head>
<title>Dekranasda Kayong Utara</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/favicon.ico">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/form.css" rel="stylesheet" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery1.min.js"></script>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!--start slider -->
    <link rel="stylesheet" href="css/fwslider.css" media="all">
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>
    <script src="js/fwslider.js"></script>
<!--end slider -->
<script src="js/jquery.easydropdown.js"></script>
<!-- start details -->
<script src="js/slides.min.jquery.js"></script>
   <script>
		$(function(){
			$('#products').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				effect: 'slide, fade',
				crossfade: true,
				slideSpeed: 350,
				fadeSpeed: 500,
				generateNextPrev: true,
				generatePagination: false
			});
		});
	</script>
<link rel="stylesheet" href="css/etalage.css">
<script src="js/jquery.etalage.min.js"></script>
<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 360,
					thumb_image_height: 360,
					source_image_width: 900,
					source_image_height: 900,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
</script>
</head>
<body>
     <div class="header-top">
	   <div class="wrap">
			  <div class="header-top-left">
			  	   <div class="recent-tweet-info"><p><?php echo "Halo, &nbsp; $user"; ?></p></div>
			  	   <!--<div class="box">
   				      <select tabindex="4" class="dropdown">
							<option value="" class="label" value="">Language :</option>
							<option value="1">English</option>
							<option value="2">French</option>
							<option value="3">German</option>
					  </select>
   				    </div>
   				    <div class="box1">
   				        <select tabindex="4" class="dropdown">
							<option value="" class="label" value="">Currency :</option>
							<option value="1">$ Dollar</option>
							<option value="2">€ Euro</option>
						</select>
   				    </div>
   				    <div class="clear"></div>-->
   			 </div>
			 <div class="cssmenu">
				<ul>
					<li><a href="media.php?hal=cart">Cart</a></li> |
					<?php
		           if (!empty($_SESSION['namauser']) AND !empty($_SESSION['passuser'])){
					?>
		            <li>
		              <a href="logout.php">Logout</a>
		            </li>
		            <?php
					}

		            if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
					?>
		            <li>
		              <a href="?hal=login">Log In</a>
		            </li>
		            <?php
					}
					?>
				</ul>
			</div>
			<div class="clear"></div>
 		</div>
	</div>
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="media.php?hal=home"><img src="images/icon-new.png" alt=""/></a>
				</div>
				<div class="menu">
	            <ul class="megamenu skyblue">
			<li class="active grid"><a href="media.php?hal=home">Home</a></li>
			<li><a class="color4" href="#">Kategori</a>
				<div class="megapanel">
					<div class="row">
						<?php
							$kategori=mysql_query("select * from kategori");
							while($k=mysql_fetch_array($kategori))
							{
							echo"<div class='col1'>
								    <div class='h_nav'>
								    	<h4>$k[nama_kategori]</h4><ul>";
									  $prod=mysql_query("select * from produk where id_kategori='$k[id_kategori]' LIMIT 5 ");
									  while ($dp=mysql_fetch_array($prod))
									  {
									  echo "<li><a href='?hal=detail&id=$dp[id_produk]'>$dp[nama_produk]</a></li>";
									  }
								echo"</ul></div></div>";
							}
						?>
					  </div>
					</div>
				</li>
				<li><a class="color6" href="media.php?hal=carabeli">Cara Pembelian</a></li>
				<?php
					if($user == "Pengunjung"){
						//
					}else{
						?><li><a href="?hal=statusbyr">Konfirmasi Pembayaran</a></li><?php
					}
                  ?>
			</ul>
			</div>
		</div>
	   <div class="header-bottom-right">
         <div class="search">
				<input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
				<input type="submit" value="Subscribe" id="submit" name="submit">
				<div id="response"> </div>
		 </div>
	  <div class="tag-list">
	    <ul class="last">
	    	<li>
	    		<?php
				$sid = session_id();
				$sql = mysql_query("SELECT SUM(jumlah*harga - (jumlah*(harga*(diskon/100)))) as total,SUM(jumlah) as totaljumlah FROM orders_temp, produk
								WHERE id_session='$sid' AND orders_temp.id_produk=produk.id_produk");
					$r=mysql_fetch_array($sql);
					if ($r['totaljumlah'] != ""){
					$total_rp    = format_rupiah($r['total']);
					echo "<a href='#'>Cart($r[totaljumlah]) - Rp. $total_rp</a>";
					}
					else
					{
					echo "<a href='#'>Cart(0) - Rp. 0</a>";
					}
			  	?>
	    	</li>
	    </ul>
	  </div>
    </div>
     <div class="clear"></div>
     </div>
	</div>
	<?php
		$current_file_name = basename($_SERVER['REQUEST_URI'], ".php");
		if($current_file_name == "media.php?hal=home"){
			include "slider.php";
		}
	?>
<div class="main">
	<div class="wrap">
		<?php include "konten.php"; ?>
	</div>
   <div class="footer">
		<div class="footer-top">
			<div class="wrap">
			  <div class="section group example">
				<div class="col_1_of_2 span_1_of_2">
					<ul class="f-list">
					  <li><img src="images/2.png"><span class="f-text">Pengiriman Mudah</span><div class="clear"></div></li>
					</ul>
				</div>
				<div class="col_1_of_2 span_1_of_2">
					<ul class="f-list">
					  <li><img src="images/3.png"><span class="f-text">Hubungi kami! 08215599488 </span><div class="clear"></div></li>
					</ul>
				</div>
				<div class="clear"></div>
		      </div>
			</div>
		</div>
		<div class="footer-middle">
			<div class="wrap">
		   <div class="section group example">
			  <div class="col_1_of_f_1 span_1_of_f_1">
				 <div class="section group example">
				   <div class="col_1_of_f_2 span_1_of_f_2">
				      <!-- <h3>Facebook</h3> -->
					<!--	<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="like_box">
							<div class="fb-like-box" data-href="http://www.facebook.com/cornelia" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
						</div> -->
 				  </div>
				  <div class="col_1_of_f_2 span_1_of_f_2">
						<!-- <h3>From Twitter</h3> -->
				       <!--<div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								<p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
							</div>
							<div class="clear"> </div>
					   </div>
					   <div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								<p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
							</div>
							<div class="clear"> </div>
					  </div>-->
				</div>
				<div class="clear"></div>
		      </div>
 			 </div>
			 <div class="col_1_of_f_1 span_1_of_f_1">
			   <div class="section group example">
				 <div class="col_1_of_f_2 span_1_of_f_2">
				    <!-- <h3>Informasi</h3>
						<ul class="f-list1">
						    <li><a href="#">Duis autem vel eum iriure </a></li>
				            <li><a href="#">anteposuerit litterarum formas </a></li>
				            <li><a href="#">Tduis dolore te feugait nulla</a></li>
				             <li><a href="#">Duis autem vel eum iriure </a></li>
				            <li><a href="#">anteposuerit litterarum formas </a></li>
				            <li><a href="#">Tduis dolore te feugait nulla</a></li>
			         	</ul> -->
 				 </div>
				 <div class="col_1_of_f_2 span_1_of_f_2">
				   <h3>Kontak</h3>
						<div class="company_address">
			                <p></p>
					   		<p>Kabupaten Kayong Utara.</p>
					   		<p>Indonesia</p>
					   		<p></p>
					 	 	<p>Email: <span>dekranasada@gmail.com</span></p>

					   </div>
					   <div class="social-media">
						     <ul>
						        <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>
						    </ul>
					   </div>
				</div>
				<div class="clear"></div>
		    </div>
		   </div>
		  <div class="clear"></div>
		    </div>
		  </div>
		</div>
		<div class="footer-bottom">
			<div class="wrap">
	             <div class="copy">
			        <p>© 2018    <a href="#" target="_blank">Dekranasda</a></p>
		         </div>
				<div class="f-list2">
				 <ul>
					<li class="active"><a href="media.php?hal=about">Tentang Kami</a></li> |
					<!--<li><a href="delivery.php">Delivery & Returns</a></li> |
					<li><a href="delivery.html">Terms & Conditions</a></li> |-->
					<li><a href="media.php?hal=contact">Hubungi Kami</a></li>
				 </ul>
			    </div>
			    <div class="clear"></div>
		      </div>
	     </div>
	</div>
</body>
</html>
