<!-- start slider -->
<div id="fwslider">
    <div class="slider_container">
        <div class="slide">
            <!-- Slide image -->
                <img src="images/banner.jpg" alt=""/>
            <!-- /Slide image -->
            <!-- Texts container -->
            <div class="slide_content">
                <div class="slide_content_wrap">
                    <!-- Text title -->
                    <h4 class="title">Dekranasda Kayong Utara</h4>
                    <!-- /Text title -->

                    <!-- Text description -->
                    <p class="description">Merangkul Pengrajin Daerah</p>
                    <!-- /Text description -->
                </div>
            </div>
             <!-- /Texts container -->
        </div>
        <!-- /Duplicate to create more slides -->
        <div class="slide">
            <img src="images/banner1.jpg" alt=""/>
            <div class="slide_content">
                <div class="slide_content_wrap">
                    <h4 class="title">Dekranasda Kayong Utara </h4>
                    <p class="description">Merangkul Pengrajin Daerah</p>
                </div>
            </div>
        </div>
        <!--/slide -->
    </div>
    <div class="timers"></div>
    <div class="slidePrev"><span></span></div>
    <div class="slideNext"><span></span></div>
</div>
<!--/slider -->
